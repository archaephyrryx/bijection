open Ppxlib

type t = { proj: Ppxlib.Ast.case list; inj : Ppxlib.Ast.case list }

let rec to_pattern : expression -> pattern =
  function
  | { pexp_desc; pexp_loc; pexp_attributes; pexp_loc_stack } ->
    let ppat_desc = match pexp_desc with
    | Pexp_constant x -> Ppat_constant x
    | Pexp_tuple xs -> Ppat_tuple (List.map to_pattern xs)
    | Pexp_construct (lid, exp) -> Ppat_construct (lid, Option.map to_pattern exp)
    | Pexp_ident { txt=Lident lab; loc } -> Ppat_var { txt=lab; loc }
    | Pexp_variant (lab, expr) -> Ppat_variant (lab, Option.map to_pattern expr)
    | Pexp_array exprs -> Ppat_array (List.map to_pattern exprs)
    | Pexp_record (exprs, None) -> Ppat_record (List.map (fun (lid, exp) -> lid, to_pattern exp) exprs, Closed)
    | Pexp_lazy exp -> Ppat_lazy (to_pattern exp)
    | Pexp_open ({ popen_expr={pmod_desc=Pmod_ident lid; _}; _}, exp) -> Ppat_open (lid, to_pattern exp)
    | Pexp_constraint (exp, typ) -> Ppat_constraint (to_pattern exp, typ)
    | Pexp_open _ -> failwith "to_pattern: no pattern for non-ident open"
    | Pexp_record (_, Some _) -> failwith "to_pattern: no pattern for record update"
    | Pexp_unreachable -> failwith "to_pattern: no pattern for Pexp_unreachable"
    | Pexp_send _ -> failwith "to_pattern: no pattern for Pexp_send"
    | Pexp_object _ -> failwith "to_pattern: no pattern for Pexp_object"
    | Pexp_letexception _ -> failwith "to_pattern: no pattern for Pexp_letexception"
    | Pexp_new _ -> failwith "to_pattern: no pattern for Pexp_new"
    | Pexp_poly _ -> failwith "to_pattern: no pattern for Pexp_poly"
    | Pexp_sequence _ -> failwith "to_pattern: no pattern for Pexp_sequence"
    | Pexp_function _ -> failwith "to_pattern: no pattern for Pexp_function"
    | Pexp_override _ -> failwith "to_pattern: no pattern for Pexp_override"
    | Pexp_newtype _ -> failwith "to_pattern: no pattern for Pexp_newtype"
    | Pexp_letop _ -> failwith "to_pattern: no pattern for Pexp_letop"
    | Pexp_extension _ -> failwith "to_pattern: no pattern for Pexp_extension"
    | Pexp_letmodule _ -> failwith "to_pattern: no pattern for Pexp_letmodule"
    | Pexp_setinstvar _ -> failwith "to_pattern: no pattern for Pexp_setinstvar"
    | Pexp_coerce _ -> failwith "to_pattern: no pattern for Pexp_coerce"
    | Pexp_while _ -> failwith "to_pattern: no pattern for Pexp_while"
    | Pexp_ifthenelse _ -> failwith "to_pattern: no pattern for Pexp_ifthenelse"
    | Pexp_field _ -> failwith "to_pattern: no pattern for Pexp_field"
    | Pexp_pack _ -> failwith "to_pattern: no pattern for Pexp_pack"
    | Pexp_assert _ -> failwith "to_pattern: no pattern for Pexp_assert"
    | Pexp_for _ -> failwith "to_pattern: no pattern for Pexp_for"
    | Pexp_setfield _ -> failwith "to_pattern: no pattern for Pexp_setfield"
    | Pexp_ident _ -> failwith "to_pattern: no pattern for Pexp_ident with qualification"
    | Pexp_let _ -> failwith "to_pattern: no pattern for Pexp_fun"
    | Pexp_fun _ -> failwith "to_pattern: no pattern for Pexp_fun"
    | Pexp_apply _ -> failwith "to_pattern: no pattern for Pexp_apply"
    | Pexp_match _ -> failwith "to_pattern: no pattern for Pexp_match"
    | Pexp_try _ -> failwith "to_pattern: no pattern for Pexp_try"
    and ppat_loc = pexp_loc
    and ppat_loc_stack = pexp_loc_stack
    and ppat_attributes = pexp_attributes
  in { ppat_desc; ppat_loc; ppat_attributes; ppat_loc_stack } 


let expand_function ~loc ~path payload = 
  let empty = { proj = []; inj = [] } in
  let add_case acc = function
    | [%expr [%e? lhs] <-> [%e? rhs]] -> 
      let nat = { pc_lhs = to_pattern lhs; pc_rhs = rhs; pc_guard=None }
      and inv = { pc_lhs = to_pattern rhs; pc_rhs = lhs; pc_guard=None }
      in { proj = acc.proj @ [nat]; inj = acc.inj @ [inv] }
    | [%expr [%e? _] <=> [%e? _]] ->
      let nat = { pc_lhs = [%pat? _]; pc_rhs = [%expr failwith "non-match in projective direction"]; pc_guard=None }
      and inv = { pc_lhs = [%pat? _]; pc_rhs = [%expr failwith "non-match in injective direction"]; pc_guard=None }
      in { proj = acc.proj @ [nat]; inj = acc.inj @ [inv] }
    | [%expr [%e? _] <== [%e? _]] ->
      let inv = { pc_lhs = [%pat? _]; pc_rhs = [%expr failwith "non-match in injective direction"]; pc_guard=None }
      in { proj = acc.proj ; inj = acc.inj @ [inv] }
    | [%expr [%e? _] ==> [%e? _]] ->
      let nat = { pc_lhs = [%pat? _]; pc_rhs = [%expr failwith "non-match in projective direction"]; pc_guard=None }
      in { proj = acc.proj @ [nat]; inj = acc.inj }
    | _ -> assert false
  in
  let { proj; inj } = List.fold_left add_case empty payload in
  ignore (loc, path, proj, inj);
  [%stri module B = struct
    let proj x = match x with c -> .
    let inj y = match y with d -> .
   end][@subst let c : list = proj and d : list = inj]

let extension =
  Extension.declare
    "bijection"
    Extension.Context.Structure_item
    Ast_pattern.(single_expr_payload (elist __))
    expand_function

let rule = Context_free.Rule.extension extension

let () =
  Driver.register_transformation ~rules:[rule] "bijection"