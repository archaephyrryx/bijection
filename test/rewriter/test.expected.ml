module B =
  struct
    let proj x =
      match x with
      | 1 -> "one"
      | _ -> failwith "non-match in projective direction"
    let inj y =
      match y with
      | "one" -> 1
      | _ -> failwith "non-match in injective direction"
  end
