module type ISOTYPE = Hashtbl.HashedType

module type Bijection_intf = sig
  module X : sig type t end
  module Y : sig type t end

  val proj : X.t -> Y.t
  val inj : Y.t -> X.t
end

module type ISO = sig
  module X : ISOTYPE
  module Y : ISOTYPE

  val rules : (X.t * Y.t) list
end

module type ISO_ext = sig
  include ISO

  val pre_proj : X.t -> X.t
  val pre_inj : Y.t -> Y.t

  val proj_overflow : X.t -> Y.t
  val inj_overflow : Y.t -> X.t
end

module type Bijection_base_intf = sig
  module X : ISOTYPE
  module Y : ISOTYPE
  
  module PROJ : Hashtbl.S
  module INJ : Hashtbl.S

  type t = { proj: Y.t PROJ.t; inj: X.t INJ.t }

  val rules : t
end


module Bijection_base (I : ISO) :
  Bijection_base_intf with
    module PROJ := Hashtbl.Make(I.X) and
    module INJ := Hashtbl.Make(I.Y) and
    module X := I.X and
    module Y := I.Y
    = struct

  type t = { proj: I.Y.t Hashtbl.Make(I.X).t; inj: I.X.t Hashtbl.Make(I.Y).t }

  module PROJ = Hashtbl.Make (I.X)
  module INJ = Hashtbl.Make (I.Y)
  let empty = { proj = PROJ.create (List.length I.rules); inj=INJ.create (List.length I.rules) }
  let rules =
    let rules = empty in
    let add_rule (x, y) =
      assert (not @@ PROJ.mem rules.proj x || INJ.mem rules.inj y);
      PROJ.add rules.proj x y;
      INJ.add rules.inj y x
    in
    List.iter add_rule I.rules;
    rules
end

module Bijection (I : ISO) :
  Bijection_intf with
    module X := I.X and
    module Y := I.Y = struct
  include Bijection_base (I)
 
  module PROJ = Hashtbl.Make(I.X)
  module INJ = Hashtbl.Make(I.Y)
  let proj x =
    match PROJ.find_opt rules.proj x with
    | None -> failwith "Bijection.proj: no matching rule"
    | Some y -> y

  let inj y =
    match INJ.find_opt rules.inj y with
    | None -> failwith "Bijection.inj: no matching rule"
    | Some x -> x
end

module Bijection_ext (I : ISO_ext) :
  Bijection_intf with
    module X := I.X and
    module Y := I.Y = struct
  include Bijection_base (I)
 
  module PROJ = Hashtbl.Make(I.X)
  module INJ = Hashtbl.Make(I.Y)

  let proj x =
    let x' = I.pre_proj x in
    match PROJ.find_opt rules.proj x' with
    | None -> I.proj_overflow x'
    | Some y -> y

  let inj y =
    let y' = I.pre_inj y in
    match INJ.find_opt rules.inj y' with
    | None -> I.inj_overflow y'
    | Some x -> x
end
