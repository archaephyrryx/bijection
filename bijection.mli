(** Module type that can be tested for equality to
    simulate pattern-matching in a constructed
    isomorphism
  *)
module type ISOTYPE = Hashtbl.HashedType

(** Module type representing the exposed submodules and methods of
    an *)
module type Bijection_intf = sig
  (** LHS type of the underlying bijection *)
  module X : sig type t end

  (** RHS type of the underlying bijection *)
  module Y : sig type t end

  (** Projective map from [X.t] to [Y.t] *)
  val proj : X.t -> Y.t

  (** Injective map from [Y.t] to [X.t] *)
  val inj : Y.t -> X.t
end

(** Module signature for a basic isomorphism with only simple rules
  *
  * i.e. Literal values to and from literal values without variables
  *)
module type ISO = sig
  module X : ISOTYPE
  module Y : ISOTYPE

  (** List of rules defining preimage-image pairs of the isomorphism
    *
    * e.g. [(1,"one");(2,"two");(3,"three")] *)
  val rules : (X.t * Y.t) list
end

(** Module signature for an extended isomorphism that canonicalizes values before projection or injection,
  * and which has catch-all functions for cases that can't be made explicit in a list of rules *)
module type ISO_ext = sig
  include ISO

  val pre_proj : X.t -> X.t
  val pre_inj : Y.t -> Y.t

  val proj_overflow : X.t -> Y.t
  val inj_overflow: Y.t -> X.t
end

module Bijection (I: ISO) : Bijection_intf with module X := I.X and module Y := I.Y
module Bijection_ext (I : ISO_ext) : Bijection_intf with module X := I.X and module Y := I.Y