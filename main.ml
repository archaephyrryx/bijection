open Core
open Core_bench
open Bijection

let (<->) x y = x, y
let (<=>) x y = x, y
let (<==) x y = x, y
let (==>) x y = x, y

module type TEST = sig
  module X : sig type t end
  module Y : sig type t end
  module B : Bijection_intf with module X := X and module Y := Y
  val pattern_proj : X.t -> Y.t
  val pattern_inj : Y.t -> X.t

  val preimages : X.t list
  val images : Y.t list
end

module type SUITE = sig
  val bijective_proj_test : unit -> unit
  val bijective_inj_test : unit -> unit
  val pattern_proj_test : unit -> unit
  val pattern_inj_test : unit -> unit
end

module GenTest (T : TEST) : SUITE = struct
  let run_test test cases =
    fun () -> List.iter ~f:(fun x -> ignore (test x)) cases
  let bijective_proj_test = run_test T.B.proj T.preimages
  let bijective_inj_test = run_test T.B.inj T.images
  let pattern_proj_test = run_test T.pattern_proj T.preimages
  let pattern_inj_test = run_test T.pattern_inj T.images
end

let make_suite (module X : SUITE) = [
  "Bijective projection", X.bijective_proj_test;
  "Bijective injection", X.bijective_inj_test;
  "Pattern projection", X.pattern_proj_test;
  "Pattern injection", X.pattern_inj_test;
]

module Test_monic_unit : TEST = struct
  module X = struct
    type t = MyUnit
    let equal _ _ = true
    let hash _ = 0
  end
  module Y = Unit

  module I = struct
    module X = X
    module Y = Y
    let rules = [(X.MyUnit,())]
  end

  module B = Bijection (I)

  let pattern_proj : X.t -> Y.t =
    function
    | MyUnit -> ()

  let pattern_inj : Y.t -> X.t = function
    | () -> X.MyUnit

  let preimages : X.t list = List.init 4 ~f:(fun _ -> X.MyUnit)
  let images : Y.t list = List.init 4 ~f:(fun _ -> ())
end

module Test_multic_string : TEST = struct
  module X = struct
    type octal = [`Oct0 | `Oct1 | `Oct2 | `Oct3 | `Oct4 | `Oct5 | `Oct6 | `Oct7]
    type t = Bool of bool | Octal of octal
    let equal = Poly.(=)
    let hash = Hashtbl.hash
  end
  module Y = String

  module I = struct
    module X = X
    module Y = Y
    let rules = [
      (X.Bool true,"true");
      (X.Bool false,"false");
      (X.(Octal `Oct0), "0");
      (X.(Octal `Oct1), "1");
      (X.(Octal `Oct2), "2");
      (X.(Octal `Oct3), "3");
      (X.(Octal `Oct4), "4");
      (X.(Octal `Oct5), "5");
      (X.(Octal `Oct6), "6");
      (X.(Octal `Oct7), "7");
      ]
  end

  module B = Bijection (I)

  let pattern_proj : X.t -> Y.t =
    function
    | X.Bool true -> "true"
    | X.Bool false -> "false"
    | X.Octal `Oct0 -> "0"
    | X.Octal `Oct1 -> "1"
    | X.Octal `Oct2 -> "2"
    | X.Octal `Oct3 -> "3"
    | X.Octal `Oct4 -> "4"
    | X.Octal `Oct5 -> "5"
    | X.Octal `Oct6 -> "6"
    | X.Octal `Oct7 -> "7"

  let pattern_inj : Y.t -> X.t =
    function
    | "true" -> X.Bool true
    | "false" -> X.Bool false
    | "0" -> X.Octal `Oct0
    | "1" -> X.Octal `Oct1
    | "2" -> X.Octal `Oct2
    | "3" -> X.Octal `Oct3
    | "4" -> X.Octal `Oct4
    | "5" -> X.Octal `Oct5
    | "6" -> X.Octal `Oct6
    | "7" -> X.Octal `Oct7
    | _ -> assert false

  let preimages, images = List.unzip I.rules
end

module Test_multic_string_ppx : TEST = struct
  module X = struct
    type octal = [`Oct0 | `Oct1 | `Oct2 | `Oct3 | `Oct4 | `Oct5 | `Oct6 | `Oct7]
    type t = Bool of bool | Octal of octal
  end
  module Y = String

  [%%bijection [
      (X.Bool true <-> "true");
      (X.Bool false <-> "false");
      (X.(Octal `Oct0) <->  "0");
      (X.(Octal `Oct1) <->  "1");
      (X.(Octal `Oct2) <-> "2");
      (X.(Octal `Oct3) <-> "3");
      (X.(Octal `Oct4) <-> "4");
      (X.(Octal `Oct5) <-> "5");
      (X.(Octal `Oct6) <-> "6");
      (X.(Octal `Oct7) <-> "7");
      (assert false <== assert false);
      ]]

  let pattern_proj : X.t -> Y.t =
    function
    | X.Bool true -> "true"
    | X.Bool false -> "false"
    | X.Octal `Oct0 -> "0"
    | X.Octal `Oct1 -> "1"
    | X.Octal `Oct2 -> "2"
    | X.Octal `Oct3 -> "3"
    | X.Octal `Oct4 -> "4"
    | X.Octal `Oct5 -> "5"
    | X.Octal `Oct6 -> "6"
    | X.Octal `Oct7 -> "7"

  let pattern_inj : Y.t -> X.t =
    function
    | "true" -> X.Bool true
    | "false" -> X.Bool false
    | "0" -> X.Octal `Oct0
    | "1" -> X.Octal `Oct1
    | "2" -> X.Octal `Oct2
    | "3" -> X.Octal `Oct3
    | "4" -> X.Octal `Oct4
    | "5" -> X.Octal `Oct5
    | "6" -> X.Octal `Oct6
    | "7" -> X.Octal `Oct7
    | _ -> assert false

  let preimages, images = List.unzip [
      (X.Bool true,"true");
      (X.Bool false,"false");
      (X.(Octal `Oct0), "0");
      (X.(Octal `Oct1), "1");
      (X.(Octal `Oct2), "2");
      (X.(Octal `Oct3), "3");
      (X.(Octal `Oct4), "4");
      (X.(Octal `Oct5), "5");
      (X.(Octal `Oct6), "6");
      (X.(Octal `Oct7), "7");
      ]
end






let suites = [
  "Monic", make_suite (module GenTest (Test_monic_unit));
  "Multic", make_suite (module GenTest (Test_multic_string));
  "Multic_ppx", make_suite (module GenTest (Test_multic_string_ppx))
  ]

let () =
  List.concat_map suites ~f:(fun (name,tests) -> List.map tests ~f:(fun (name', test) -> Bench.Test.create ~name:(name ^ ">" ^ name') test))
  |> Bench.make_command
  |> Command.run
